$(document).ready(function () {
	var sideslider = $('[data-toggle=collapse-side]');
	var sel = sideslider.attr('data-target');
	var sel2 = sideslider.attr('data-target-2');
	sideslider.click(function (event) {
		$(sel).toggleClass('in');
		$(sel2).toggleClass('out');
	});
	$('.downOpen').click(function () {

		if ($(this).hasClass('open_1')) {
			$('body').removeClass('enablebg');
			$(this).removeClass('open_1 glyphicon-menu-down'); $(this).addClass('close_1 glyphicon-menu-up'); $(this).parent('.bar').css('height', '55px'); $(this).find('detail').class('display', 'none');

		} else {
			$('body').addClass('enablebg');
			$(this).addClass('open_1 glyphicon-menu-down'); $(this).removeClass('close_1 glyphicon-menu-up'); $(this).parent('.bar').css('height', '350px'); $(this).parent().find('.detail').css('display', 'block');
		}
	});

	$(document).ready(function() {
  
		$.ajax({
		  url: "https://cors-anywhere.herokuapp.com/https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJuVnRajzU3ogRpdz30sTIqkc&key=AIzaSyCCGb_0fod9vxW27A5iuYwNbW_x2JiCAvc",
		  dataType: "json",
		  type: "GET", 
		
		  success: function(json) {
			
			$.each(json.result.reviews, function(i,v) {
			  if (i == 4) {
				  return false;
			  }
			  var rating = v.rating;
			  if (rating == 5) {
				var starRating = "<i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i>";
			  } else if (rating == 4) {
				var starRating = "<i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i>";
			  }
			  else if (rating == 3) {
				var starRating = "<i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i>";
			  }
			  else if (rating == 2) {
				var starRating = "<i class='fa fa-star' aria-hidden='true'></i><i class='fa fa-star' aria-hidden='true'></i>";
			  }
			  else {
				var starRating = "<i class='fa fa-star' aria-hidden='true'></i>";
			  }
			  
			  
			  $('#google-reviews').append("<div class='review-wrap'>"
				+"<div class='author'>"
				+"<img class='author-img' src='"+v.profile_photo_url+"' alt='"+v.author_name+"' />"
				+"<div class='author-name'>"+v.author_name+"</div>"
				+"</div>"
				+"<div class='review'>"+v.text+"</div>"
				+"<div class='rating d-flex'>"+starRating+"</div>"
				+"</div>");
			});
		},
		error: function(xhr, status, errorThrown) {
		  //do something if there was an error. Right now it will just show the default values in the html
		}
	   });
	  });
});
$('.date-list .date-item').click(function () {
	$('.date-list .date-item').removeClass('active');
	$(this).addClass('active');
})
$('.time-list li').click(function () {
	$('.time-list li').removeClass('active');
	$(this).addClass('active');
})
 

// iphone navigation

// my booking

//======= Main Tabs =======//

// Toggle My Booking tab
var tab1 = $("a[href='#bookingbar-check-in']").first()
tab1.click(function () {

	$("a[href='#bookingbar-flight'], a[href='#bookingbar-status']").removeClass("is-active");
	$(this).addClass("is-active");
	$("#bookingbar-check-in .l-horizontal.bookingbar-content").show();
	$("#bookingbar-check-in").show();
	$("#bookingbar-flight").hide();
	$("#bookingbar-status").hide();
});

// close and back to Flight
$(document).on('click', ".js-bookingbar-options-close", function (e) {
	$("a[href='#bookingbar-check-in'], a[href='#bookingbar-status']").removeClass("is-active");
	$("a[href='#bookingbar-flight']").addClass("is-active");
	$("#bookingbar-flight").show();
	$("#bookingbar-check-in").hide();
	$("#bookingbar-status").hide();
});

// Toggle Flight tab
$(document).on('click', "a[href='#bookingbar-flight']", function (e) {
	$("a[href='#bookingbar-check-in'], a[href='#bookingbar-status']").removeClass("is-active");
	$(this).addClass("is-active");
	$("#bookingbar-flight").show();
	$("#bookingbar-check-in").hide();
	$("#bookingbar-status").hide();
});


// Toggle Status tab
$(document).on('click', "a[href='#bookingbar-status']", function (e) {
	$("a[href='#bookingbar-flight'], a[href='#bookingbar-check-in']").removeClass("is-active");
	$(this).addClass("is-active");
	$("#bookingbar-status .l-horizontal.bookingbar-content").show();
	$("#bookingbar-status").show();
	$("#bookingbar-flight").hide();
	$("#bookingbar-check-in").hide();
});




//======= Options =======//

//Open Flight options
$(document).on('focus click', 'input#OriginLoc, input#Location, input#DestinationLoc, input#RentalLocation, .js-bookingbar-options-open', function (e) {
	event.preventDefault();
	$("#bookingbar-flight").removeClass("is-options-hidden")
});

// Close Flight options
$(document).on('click', ".bookingbar-close-options", function (e) {
	$("#bookingbar-flight").addClass("is-options-hidden")
});

// Toggle options sub-tabs
$(document).on('click', "a[href='#bookingbar-hotel-subtab']", function (e) {
	$(this).addClass("is-active");
	$("a[href='#bookingbar-flight-subtab'], a[href='#bookingbar-car-subtab']").removeClass("is-active");
	$("#bookingbar-hotel-subtab").show();
	$("#bookingbar-flight-subtab").hide();
	$("#bookingbar-car-subtab").hide()
});

$(document).on('click', "a[href='#bookingbar-flight-subtab']", function (e) {
	$(this).addClass("is-active");
	$("a[href='#bookingbar-hotel-subtab'], a[href='#bookingbar-car-subtab']").removeClass("is-active");
	$("#bookingbar-flight-subtab").show();
	$("#bookingbar-hotel-subtab").hide();
	$("#bookingbar-car-subtab").hide()
});

$(document).on('click', "a[href='#bookingbar-car-subtab']", function (e) {
	$(this).addClass("is-active");
	$("a[href='#bookingbar-hotel-subtab'], a[href='#bookingbar-flight-subtab']").removeClass("is-active");
	$("#bookingbar-car-subtab").show();
	$("#bookingbar-hotel-subtab").hide();
	$("#bookingbar-flight-subtab").hide()
});

$('.checkout .item').click(function(){
	$('.checkout .item').removeClass('item-selected');
	$('.checkout .item').find('.checkout-form').slideUp();
	$(this).addClass('item-selected');
	$(this).find('.checkout-form').slideDown();
	

})


//======= Flight status =======//

$("#bookingbar-status div.tabs-navigation").addClass("tabs-navigation--is-menu");



$("html").click(function () {
	$("#searchSelectDropDown").hide();
	$(".tabs-navigation--is-menu").click(function (event) {
		event.stopPropagation();
		$("#searchSelectDropDown").show();
	});
});


// Search by flight
$(document).on('click', "a[href='#bookingbar-flight-number-subtab']", function (e) {
	$(".tabs-navigation--is-menu a").html("Search by flight");
	$("a[href='#bookingbar-airport-subtab'], a[href='#bookingbar-route-subtab']").removeClass("selected");
	$(this).addClass("selected");
	$("#searchSelectDropDown").hide();
	$("#bookingbar-airport-subtab").hide();
	$("#bookingbar-route-subtab").hide();
	$("#bookingbar-flight-number-subtab").show();
});

// Search by route
$(document).on('click', "a[href='#bookingbar-route-subtab']", function (e) {
	$(".tabs-navigation--is-menu a").html("Search by route");
	$("a[href='#bookingbar-airport-subtab'], a[href='#bookingbar-flight-number-subtab']").removeClass("selected");
	$(this).addClass("selected");
	$("#searchSelectDropDown").hide();
	$("#bookingbar-airport-subtab").hide();
	$("#bookingbar-route-subtab").show();
	$("#bookingbar-flight-number-subtab").hide();
});

// Search by airport
$(document).on('click', "a[href='#bookingbar-airport-subtab']", function (e) {
	$(".tabs-navigation--is-menu a").html("Search by airport");
	$("a[href='#bookingbar-route-subtab'], a[href='#bookingbar-flight-number-subtab']").removeClass("selected");
	$(this).addClass("selected");
	$("#searchSelectDropDown").hide();
	$("#bookingbar-airport-subtab").show();
	$("#bookingbar-route-subtab").hide();
	$("#bookingbar-flight-number-subtab").hide();
});



//======= Autocomplete =======//
// https://github.com/devbridge/jQuery-Autocomplete

var countries = [
	{ value: 'Paris (PAR)', data: '0' },
	{ value: 'Casablanca (CAS)', data: '1' },
	{ value: 'New York (NYC)', data: '2' },
	{ value: 'Sao Paulo (SAO)', data: '3' },
	{ value: 'Paris Charles De Gaulle (CDG)', data: '4' },
	{ value: 'Karlsruhe/Baden Baden Baden Airpark (FKB)', data: '5' },
	{ value: 'Mataram Selaparang (AMI)', data: '6' }
];

//const newLocal = $('#OriginLoc, #DestinationLoc').autocomplete({
//lookup: countries,
//onSelect: function (suggestion) {
//	console.log('You selected: ' + suggestion.value + ', ' + suggestion.data);
//}
//});


//for new header navigation
if (window.navigator.standalone == true) {
	$('body').addClass('ios-web-app');
}

if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	$('body').addClass('ios');
}

// $(window).on('load resize', function () {
// 	if ($(document).width() <= 600) {
// 		$('input').attr('placeholder', 'Search');
// 	} else {
// 		$('input').attr('placeholder', 'Search Zayn..');
// 	}
// });


$('header .menu, .menu-overlay').on('click', function () {
	$('main, nav').toggleClass('opened');
	featured();
	cards();
	genres();
})


$('nav li').on('click', function () {
	$('nav li').removeClass('active');
	$(this).addClass('active');
});


$('header .search-btn, .header .back').on('click', function () {
	$('header .mobile-search').toggleClass('searching');
	$('header .mobile-search input').focus()
})

// PROFILE MENU

$('.profile-tikla').on('click', function () {
	$('.account').toggleClass('active');
});

$(document).on("click", function (e) {
	if (($(".account").hasClass("active")) && (!$(".account, .account *, .profile-tikla").is(e.target))) {
		$(".account").toggleClass("active");
	}
});

$(window).scroll(function () {
	$('.account').removeClass('active');
});


// Ripple Code 

$(document).on('click', '.ripple', function (e) {

	var $ripple = $('<span class="rippling" />'),
		$button = $(this),
		btnOffset = $button.offset(),
		xPos = e.pageX - btnOffset.left,
		yPos = e.pageY - btnOffset.top,
		size = 1,
		animateSize = parseInt(Math.max($button.innerWidth(), $button.innerHeight()) * Math.PI);

	$ripple.css({
		top: yPos,
		left: xPos,
		width: size,
		height: size,
		backgroundColor: $button.attr("ripple-color"),
		opacity: $button.attr("ripple-opacity")
	})
		.appendTo($button)
		.animate({
			width: animateSize,
			height: animateSize,
			// transform: 'scale(' + animateSize + ')',
			opacity: 0
		}, 500, function () {
			$(this).remove();
		});
});


