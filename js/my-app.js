"use strict";

/*===============================================*/
/* APP INIT                                          */
/*===============================================*/
var myApp = new Framework7({
    material: true,
    init: false,
    swipePanel: "left"
});

/*===============================================*/
/* EXPORT SELECTORS ENGINE                          */
/*===============================================*/
var $$ = Dom7;

/*===============================================*/
/* ADD VIEW                                           */
/*===============================================*/
var mainView = myApp.addView(".view-main", {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

/*=========================================================*/
/* SHOW/HIDE PRELOADER FOR REMOTE AJAX LOADED PAGES           */
/*=========================================================*/
$$(document).on("ajaxStart", function (e) {
    myApp.showIndicator();
});
$$(document).on("ajaxComplete", function () {
    myApp.hideIndicator();
});

/*==================================================================*/
/* PAGE INIT : HERE, YOU CAN WRITE YOUR CUSTOM JAVASCRIPT/JQUERY    */
/*==================================================================*/
$$(document).on("pageInit", function (e) {
    /* SLIDE SLICK */
    /*================================*/
    var slickOpts = {
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        dots: true,
        centerMode: true,
        centerPadding: "15px",
        adaptiveHeight: true
    };
    $("#walkthrough-items").slick(slickOpts);

    /* CALENDAR */
   /*================================*/
    var calendarDefault = myApp.calendar({
        input: "#calendar-default"
    });
    var pickerDescribe = myApp.picker({
        input: "#picker-time",
        cols: [
            {
                textAlign: "left",
                values: (function () {
                    var arr = [];
                    for (var i = 0; i <= 23; i++) { arr.push(i); }
                    return arr;
                })(),
            },
            {
                values: ("PM AM").split(" ")
            },
        ]
    }); 

    $(".category-select").click(function(){
        var index = $(event.currentTarget).data("cat-index");
        if(index){
            var category = $('.category-section')[index - 1];

            $('.page-content').animate({
                scrollTop: category.offsetTop-56
            }, 500);
        }
        
    });

    $('.page-nav nav a').click(function(event){
        $('.page-nav nav a').removeClass('active');
			$(event.currentTarget).addClass('active');
    });

    $(".page-content").scroll(function(pos) {
        var navLinks = document.querySelectorAll('.page-nav nav a');
        for(var i=0; i<$('.category-section').length; i++){
            var category = $('.category-section')[i];
            
            var hT = category.offsetTop,
            hH = category.offsetHeight,
            wS = $(this).scrollTop();  // top of scrollable area 
            
            if(wS < $('.category-section')[0].offsetTop-56){
                $(".categoryHeader").hide();
            } else {
                $(".categoryHeader").show()
            }

            if (wS >= hT-56 &&  wS <= hT+hH-56 ){
                $(".categoryHeader").text(category.dataset.cat);
                translateNav(i+1);
            }
        }


            

        
    });
});

// AND NOW WE INITIALIZE APP
myApp.init();

var navItemsAnimation;

function translateNav(item) {
    // If animation is defined, pause it
    var navHeight = 40;
    if (navItemsAnimation) navItemsAnimation.pause();
    // Animate the `translateY` of `nav` to show only the current link
    navItemsAnimation = anime({
        targets: $('.page-nav nav'),
        translateY: (item ? item * navHeight : 0) + 'px',
        easing: 'easeOutCubic',
        duration: 500
    });
    // Update link on arrows, and disable/enable accordingly if first or last link
    //updateArrows();
}




